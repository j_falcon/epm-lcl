﻿namespace Dbsoft.Epm.Web.Infrastructure
{
    using AutoMapper;
    using Controllers.Maintenance;
    using Controllers.Production;
    using Controllers.Production.InboundContract;
    using Controllers.Production.PostBuyOrders;
    using Controllers.Production.PostSellOrders;
    using Controllers.Production.UpdateBuyOrders;
    using Controllers.Production.UpdateSellOrders;
    using Controllers.Reporting.DailySales;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services.AccountApi;
    using DBSoft.EPM.DAL.Services.Contracts;
    using DBSoft.EVEAPI.Entities.MarketOrder;
    using MarketOrder = DBSoft.EVEAPI.Entities.MarketOrder.MarketOrder;

    public static class MapperConfig
    {
        public static void ConfigureMappings()
        {
            Mapper.CreateMap<Station, StationDTO>()
                .ForMember(m => m.StationID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.StationName, opt => opt.MapFrom(m => m.Name))
                .ForMember(m => m.StationTax, opt => opt.MapFrom(m => m.Tax))
                ;
            Mapper.CreateMap<MarketPrice, MarketAdjustedPriceDTO>();
            Mapper.CreateMap<SolarSystem, SolarSystemDTO>()
                .ForMember(m => m.SolarSystemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.SolarSystemName, opt => opt.MapFrom(m => m.Name))
                ;

            Mapper.CreateMap<UserDTO, ApplicationUser>();
            Mapper.CreateMap<User, UserDTO>()
                .ForMember(m => m.UserID, map => map.MapFrom(m => m.ID))
                .ForMember(m => m.UserName, map => map.MapFrom(m => m.EveOnlineCharacter));

            Mapper.CreateMap<MaterialItemDto, MaterialItemModel>();

            Mapper.CreateMap<ProductionMaterialDto, ProductionMaterialItemModel>()
                .ForMember(m => m.Available, opt => opt.MapFrom(m => m.Inventory))
                .ForMember(m => m.Needed, opt => opt.MapFrom(m => m.Required));

            Mapper.CreateMap<MarketOrder, SaveMarketOrderRequest>()
                .ForMember(m => m.OrderID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.OriginalQuantity, opt => opt.MapFrom(m => m.VolumeEntered))
                .ForMember(m => m.RemainingQuantity, opt => opt.MapFrom(m => m.VolumeRemaining))
                .ForMember(m => m.MinimumQuantity, opt => opt.MapFrom(m => m.MinimumVolume))
                .ForMember(m => m.OrderStatus,
                    opt =>
                        opt.ResolveUsing(
                            f => f.OrderState == OrderState.Active ? OrderStatus.Active : OrderStatus.Inactive))
                .ForMember(m => m.EveCharacterID, opt => opt.MapFrom(m => m.CharacterID))
                .ForMember(m => m.Token, opt => opt.Ignore())
                ;

            Mapper.CreateMap<ProductionQueueDto, ProductionQueueItemModel>();

            Mapper.CreateMap<InboundContractDTO, InboundContractItemModel>();

            Mapper.CreateMap<MarketRestockDTO, PostSellOrdersItemModel>();

            Mapper.CreateMap<MarketRepriceDTO, UpdateSellOrderItemModel>();

            Mapper.CreateMap<MaterialPurchaseDto, PostBuyOrderItemModel>();

            Mapper.CreateMap<MaterialPurchaseDto, UpdateBuyOrderItemModel>();

            Mapper.CreateMap<ItemTransactionByDateDto, DailySaleItemModel>();
            Mapper.CreateMap<ItemTransactionByMonthDto, DailySaleItemModel>();

            Mapper.CreateMap<Item, ItemDTO>()
                .ForMember(f => f.ItemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(f => f.ItemName, opt => opt.MapFrom(m => m.Name))
                ;

            Mapper.CreateMap<ConfigurationModel, ConfigurationSettingsDTO>()
                .ForMember(m => m.FactoryLocation, opt => opt.MapFrom(m => m.FactoryID))
                .ForMember(m => m.MarketSellLocation, opt => opt.MapFrom(m => m.MarketSellID))
                .ForMember(m => m.MarketBuyLocation, opt => opt.MapFrom(m => m.MarketBuyID))
                .ForMember(m => m.PosLocation, opt => opt.MapFrom(m => m.PosLocationID))
                .ForMember(m => m.EveOnlineCharacter, opt => opt.Ignore())
                ;

            Mapper.CreateMap<ConfigurationSettingsDTO, ConfigurationModel>()
                .ForMember(m => m.FactoryID, opt => opt.MapFrom(m => m.FactoryLocation))
                .ForMember(m => m.MarketSellID, opt => opt.MapFrom(m => m.MarketSellLocation))
                .ForMember(m => m.MarketBuyID, opt => opt.MapFrom(m => m.MarketBuyLocation))
                .ForMember(m => m.PosLocationID, opt => opt.MapFrom(m => m.PosLocation))
                .ForMember(m => m.ConfigurationValid, opt => opt.Ignore())
                .ForMember(m => m.RegionID, opt => opt.Ignore())
                ;

            Mapper.CreateMap<Item, ItemDTO>()
                .ForMember(f => f.ItemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(f => f.ItemName, opt => opt.MapFrom(m => m.Name))
                ;
            Mapper.CreateMap<AccountDTO, AccountModel>().ReverseMap();
        }
    }
}
