﻿namespace Dbsoft.Epm.Web.Infrastructure.Filters
{
    using System.Security.Authentication;
    using Microsoft.AspNet.Mvc;

    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is AuthenticationException)
            {
                context.Result = new RedirectResult("~/Account/Logout");
            }
        }

    
    }
}
