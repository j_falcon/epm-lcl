namespace Dbsoft.Epm.Web.Infrastructure
{
    using Autofac;
    using DBSoft.EVEAPI.Entities.WalletTransaction;
    using DBSoft.EVEAPI.Plumbing;

    public class MockModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MockContentLoader>().As<IEveContentLoader>();
            builder.RegisterType<MockTransactionNormalizer>().As<ITransactionNormalizer>();
        }
    }
}