namespace DBSoft.EVEAPI.Crest.MarketOrder
{
    using Entities.MarketOrder;

    public class MarketOrderDTO
    {
        public long OrderID { get; set; }
        public int StationID { get; set; }
        public double Price { get; set; }
        public int ItemID { get; set; }
        public OrderType OrderType { get; set; }
        public short Range { get; set; }
    }
}