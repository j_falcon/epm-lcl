﻿namespace DBSoft.EVEAPI.Crest.MarketOrder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Entities;
    using Entities.MarketOrder;
    using Newtonsoft.Json;

    public class MarketService : IMarketService
    {
        private const string BaseUri = "https://crest-tq.eveonline.com";

        public List<MarketOrderDTO> ListOrders(MarketOrderRequest request)
        {
            var listOfLists = request.ItemIds.ToList();
            var tasks = listOfLists.Select(batch => Task.Run(() => LoadMarketOrders(request, batch))).ToList();
            var list = new List<MarketOrderDTO>();
            var result = Task.WhenAll(tasks);
            foreach (var r in result.Result)
            {
                list.AddRange(r);
            }
            return list;
        }

        public List<MarketHistoryDTO> ListHistory(MarketHistoryRequest request)
        {
            var listOfLists = request.ItemIds.ToList();
            var tasks = listOfLists.Select(id => Task.Run(() => LoadMarketHistory(request, id))).ToList();
            var result = Task.WhenAll(tasks);
            return result.Result.ToList();
        }

        public List<MarketSummaryDTO> ListSummaries(MarketSummaryRequest request)
        {
            var listOfLists = request.ItemIDs.ToList();
            var tasks = listOfLists.Select(id => Task.Run(() => LoadMarketSummaries(request, id))).ToList();
            var result = Task.WhenAll(tasks);
            return result.Result.ToList();
        }

        private static MarketSummaryDTO LoadMarketSummaries(MarketSummaryRequest request, int id)
        {
            while ( true )
            {
                var url = string.Format("{3}/market/{0}/orders/{1}/?type=https://api.eveonline.com/types/{2}/", request.RegionId,
                    request.OrderType.ToString().ToLower(), id, BaseUri);

                try
                {
                    var readToEnd = JsonLoader.Load(url, request.Token);
                    var result = JsonConvert.DeserializeObject<MarketOrderResponse>(readToEnd);
                    var dto = new MarketSummaryDTO
                    {
                        ItemID = id,
                        ItemName = result.items.First().type.name,
                        MinimumSellPrice = request.OrderType == OrderType.Sell ?
                            (decimal)result.items.Where(f => f.location.id == request.StationID && !f.buy).Min(f => f.price) : 0,
                        SellVolume = result.items.Where(f => f.location.id == request.StationID).Sum(f => f.volume),
                        MaximumBuyPrice = request.OrderType == OrderType.Buy ?
                            (decimal)result.items.Where(f => f.location.id == request.StationID && f.buy).Max(f => f.price) : 0,
                        Competitors = Math.Max(result.items.Where(IsActive).Count(), 1)
                    };
                    return dto;
                }
                catch (Exception e)
                {
                    if (e.Message != "The operation has timed out")
                    {
                        return new MarketSummaryDTO
                        {
                            ItemID = id,
                            Exception = e.Message
                        };
                    }
                }
            }
        }

        private static bool IsActive(Item item)
        {
            return DateTime.Parse(item.issued) > DateTime.UtcNow.AddDays(-1);
        }

        private static MarketHistoryDTO LoadMarketHistory(MarketHistoryRequest request, int id)
        {
            var url = string.Format("{2}/market/{0}/types/{1}/history/", request.RegionID, id, BaseUri);
            try
            {
                var readToEnd = JsonLoader.Load(url, request.Token);
                var result = JsonConvert.DeserializeObject<MarketHistoryResponse>(readToEnd);
                return new MarketHistoryDTO
                {
                    ItemID = id,
                    AverageDailySales = result.items.Where(f => f.date > DateTime.UtcNow.AddDays(-31)).Sum(f => f.volume / 30)
                };
            }
            catch (Exception e)
            {
                return new MarketHistoryDTO
                {
                    ItemID = id,
                    Exception = e.Message
                };
            }
        }

        private static List<MarketOrderDTO> LoadMarketOrders(MarketOrderRequest request, int id)
        {
            var url = string.Format("{3}/market/{0}/orders/{1}/?type=https://api.eveonline.com/types/{2}/",
                     request.RegionId, request.OrderType.ToString().ToLower(), id, BaseUri);
            var readToEnd = JsonLoader.Load(url, request.Token);
            var result = JsonConvert.DeserializeObject<MarketOrderResponse>(readToEnd);
            var list = result.items.Select(f => new MarketOrderDTO
            {
                ItemID = f.type.id,
                OrderID = f.id,
                OrderType = f.buy ? OrderType.Buy : OrderType.Sell,
                Price = f.price,
                StationID = f.location.id,
                Range = GetRange(f.range)
            }).ToList();
            return list;
        }

        private static short GetRange(string range)
        {
            switch (range)
            {
                case "region":
                    return 32767;
                case "station":
                case "solarsystem":
                    return 0;
                default:
                    return short.Parse(range);
            }
        }
    }
}
