namespace DBSoft.EVEAPI.Crest.MarketOrder
{
    using System.Collections.Generic;

    public interface IMarketService
    {
        List<MarketOrderDTO> ListOrders(MarketOrderRequest request);
        List<MarketHistoryDTO> ListHistory(MarketHistoryRequest request);
        List<MarketSummaryDTO> ListSummaries(MarketSummaryRequest request);
    }
}