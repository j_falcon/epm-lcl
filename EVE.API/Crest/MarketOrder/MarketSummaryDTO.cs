namespace DBSoft.EVEAPI.Crest.MarketOrder
{
    public class MarketSummaryDTO
    {
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public long SellVolume { get; set; }
        public decimal MinimumSellPrice { get; set; }
        public decimal MaximumBuyPrice { get; set; }
        public string Exception { get; set; }
        public int Competitors { get; set; }
    }
}