namespace DBSoft.EVEAPI.Crest.MarketOrder
{
    using System.Collections.Generic;
    using System.Linq;
    using Annotations;

    [UsedImplicitly]
    public class MockMarketService : IMarketService
    {
        public List<MarketOrderDTO> ListOrders(MarketOrderRequest request)
        {
            return request.ItemIds.Select(item => new MarketOrderDTO
            {
                ItemID = item,
                StationID = 60008170
            }).ToList();
        }

        public List<MarketHistoryDTO> ListHistory(MarketHistoryRequest request)
        {
            return request.ItemIds.Select(item => new MarketHistoryDTO
            {
                ItemID = item,
                AverageDailySales = 1
            }).ToList();
        }

        public List<MarketSummaryDTO> ListSummaries(MarketSummaryRequest request)
        {
            return request.ItemIDs.Select(item => new MarketSummaryDTO
            {
                ItemID = item,
                Competitors = 1,
                SellVolume = 1,
                MinimumSellPrice = 1,
                MaximumBuyPrice = 1
            }).ToList();
        }
    }
}