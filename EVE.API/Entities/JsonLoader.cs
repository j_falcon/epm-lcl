﻿namespace DBSoft.EVEAPI.Entities
{
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class JsonLoader
    {
        public static string Load(string url, string token = null)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            if ( token != null )
            {
                req.Headers.Add("Authorization", "Bearer " + token);
                // req.Accept = "application/vnd.ccp.eve.MarketOrderCollection-v1+json";
                req.ContentType = "application/x-www-form-urlencoded";
            }
            var response = req.GetResponse();
            using (var stream = response.GetResponseStream())
            {
                if (stream == null) return null;
                using (var buffer = new BufferedStream(stream))
                {
                    var reader = new StreamReader(buffer);
                    return reader.ReadToEnd();
                }
            }
        }

        public async static Task<string> LoadAsync(string url, string token = null)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Bearer", token);
            var response = await client.GetAsync(url);
            return await response.Content.ReadAsStringAsync();
        }
    }
}