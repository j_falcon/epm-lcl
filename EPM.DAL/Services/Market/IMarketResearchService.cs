namespace DBSoft.EPM.DAL.Services.Market
{
    using System.Collections.Generic;

    public interface IMarketResearchService
    {
        List<MarketResearchDTO> List(string token);
        void Update(string token, int stationId);
    }
}