namespace DBSoft.EPM.DAL.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DTOs;

    public interface IUniverseService
    {
        Task<IEnumerable<RegionDTO>> ListRegions();
        List<SolarSystemDTO> ListSolarSystems();
        StationDTO GetStation(int stationId);
        SolarSystemDTO GetStationSolarSystem(int stationId);
        List<MarketAdjustedPriceDTO> GetAdjustedMarketPrices();
        IEnumerable<StationDTO> ListStations();
    }
}