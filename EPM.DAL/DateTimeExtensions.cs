﻿namespace DBSoft.EPM.DAL
{
	using System;

	public static class DateTimeExtensions
	{
		public static DateTime StartOfTheDay(this DateTime dt)
		{
			return dt.Date;
		}

		public static DateTime EndOfTheDay(this DateTime dt)
		{
			return dt.Date.AddDays(1).AddTicks(-1);
		}

		public static DateTime StartOfTheMonth(this DateTime dt)
		{
			return new DateTime(dt.Year, dt.Month, 1);
		}

		public static DateTime EndOfTheMonth(this DateTime dt)
		{
			return dt.StartOfTheMonth().AddMonths(1).AddTicks(-1);
		}
	}
}
