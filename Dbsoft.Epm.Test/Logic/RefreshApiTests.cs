﻿namespace Dbsoft.Epm.Test.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.AccountApi;
    using DBSoft.EPM.DAL.Services.ItemCosts;
    using DBSoft.EPM.DAL.Services.Materials;
    using DBSoft.EPM.DAL.Services.Transactions;
    using DBSoft.EPM.Logic;
    using DBSoft.EPM.Logic.Config;
    using DBSoft.EPM.Logic.RefreshApi;
    using DBSoft.EVEAPI.Crest;
    using DBSoft.EVEAPI.Crest.MarketOrder;
    using DBSoft.EVEAPI.Entities.Account;
    using DBSoft.EVEAPI.Entities.MarketOrder;
    using DBSoft.EVEAPI.Entities.WalletTransaction;
    using Microsoft.Framework.OptionsModel;
    using NSubstitute;
    using Xunit;
    using MarketOrderDTO = DBSoft.EVEAPI.Crest.MarketOrder.MarketOrderDTO;

    public class RefreshApiTests
    {
        protected readonly IEveApiStatusService Status;
        protected IConfigurationService Config;

        public RefreshApiTests()
        {
            Status = Substitute.For<IEveApiStatusService>();
        }

        [Fact]
        public void MarketImportMapper_ImportsMaterialSellPrices()
        {
            var items = Substitute.For<IItemService>();
            items.ListBuildable(Arg.Any<string>()).Returns(new List<BuildableItemDTO>());

            Config = Substitute.For<IConfigurationService>();
            var universe = Substitute.For<IUniverseService>();
            universe.GetStationSolarSystem(Arg.Any<int>()).Returns(new SolarSystemDTO());
            var users = Substitute.For<IUserService>();
            users.GetAuthenticatedUser(Arg.Any<string>()).Returns(new UserDTO());
            var options = Substitute.For<IOptions<Authentication>>();
            options.Options.Returns(new Authentication { EveSso = new EveSso() });
            var auth = Substitute.For<IUserAuth>();
            auth.RefreshAuthenticatedUser(null, null, null)
                .ReturnsForAnyArgs(Task.Run(() => new AuthenticatedUserDTO()));
            var market = Substitute.For<IMarketService>();
            var buy = new List<MarketOrderDTO> { new MarketOrderDTO { OrderType = OrderType.Buy } };
            market.ListOrders(Arg.Is<MarketOrderRequest>(f => f.OrderType == OrderType.Buy))
                .Returns(buy);
            var sell = new List<MarketOrderDTO> { new MarketOrderDTO { OrderType = OrderType.Sell } };
            market.ListOrders(Arg.Is<MarketOrderRequest>(f => f.OrderType == OrderType.Sell))
                .Returns(sell);
            var materials = Substitute.For<IMaterialItemService>();
            materials.ListBuildable(Arg.Any<string>()).Returns(new List<MaterialItemDto>());
            var importer = Substitute.For<IMarketImportService>();
            var jumps = Substitute.For<IJumpService>();
            var mapper = new MarketImportMapper(market, materials, items, Config, universe, importer, Status, jumps,
                options, auth, users);
            mapper.Pull("").Wait();
            importer.Received()
                .SaveMarketImports(Arg.Any<string>(), Arg.Any<DateTime>(),
                    Arg.Is<List<SaveMarketImportRequest>>(
                        f =>
                            f.Any(any => any.OrderType == DBSoft.EPM.DAL.CodeFirst.Models.OrderType.Buy) &&
                            f.Any(any => any.OrderType == DBSoft.EPM.DAL.CodeFirst.Models.OrderType.Sell)));
        }

    }

    public class TransactionMapperTests : RefreshApiTests
    {
        private readonly TransactionMapper _mapper;
        private readonly ITransactionService _transactions;
        private readonly IItemCostService _costs;
        private const int Raven = 638;
        private const int Rokh = 24688;

        public TransactionMapperTests()
        {
            _transactions = Substitute.For<ITransactionService>();
            var accounts = Substitute.For<IAccountApiService>();
            accounts.List(Arg.Any<string>()).Returns(new List<AccountApiDTO> {new AccountApiDTO()});
            var wallet = Substitute.For<IWalletTransactionService>();
            wallet.Load(ApiKeyType.Character, 0, "", 0, 0, 0)
                .ReturnsForAnyArgs(Task.Run(() => new ApiLoadResponse<WalletTransaction>
                {
                    Data = new List<WalletTransaction>
                    {
                        new WalletTransaction
                        {
                            Type = "sell",
                            TypeID = Raven
                        },
                        new WalletTransaction
                        {
                            Type = "sell",
                            TypeID = Rokh
                        }
                    }
                }));
            _costs = Substitute.For<IItemCostService>();
            _costs.ListBuildable(Arg.Any<ListBuildableRequest>()).Returns(new List<ItemCostDTO>
            {
                new ItemCostDTO(Config) { ItemID = Raven}
            });
            var items = Substitute.For<IItemService>();
            items.ListBuildable(Arg.Any<string>()).Returns(new List<BuildableItemDTO>
            {
                new BuildableItemDTO
                {
                    ItemID = Raven
                }
            });
            _mapper = new TransactionMapper(wallet, Status, _costs, accounts, _transactions, items);
        }

        [Fact]
        public async Task WhenCostNotFound_Throws()
        {
            _costs.ListBuildable(Arg.Any<ListBuildableRequest>()).Returns(new List<ItemCostDTO>());
            await Assert.ThrowsAsync<MapperException>(async () => { await _mapper.Pull(null); });
        }

        [Fact]
        public void WhenTrackedSale_SavesVisible()
        {
            _mapper.Pull("").Wait();
                _transactions.Received()
                    .SaveTransactions(Arg.Any<string>(),
                        Arg.Is<IEnumerable<SaveTransactionRequest>>(f => f.Any(a => a.ItemID == Raven && a.VisibleFlag)));
        }

        [Fact]
        public void WhenUntrackedSale_SavesHidden()
        {
            // Not sure why saving hidden untracked items. We need hidden buy items for costing, but the only real
            // concern here is that the Rokh doesn't show for whichever reason
            _mapper.Pull("").Wait();
            _transactions.Received()
                .SaveTransactions(Arg.Any<string>(),
                    Arg.Is<IEnumerable<SaveTransactionRequest>>(f => !f.Any(a => a.ItemID == Rokh && a.VisibleFlag)));
        }
    }


}
