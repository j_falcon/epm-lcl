﻿namespace Dbsoft.Epm.Test
{
    using System.Collections.Generic;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Services.Transactions;
    using NSubstitute;
    using Web.Controllers.api;
    using Xunit;
    using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

    public class DashboardTests
    {
        [Fact]
        public void WhenNoSales_CapitalRatioShouldBeNull()
        {
            var assets = Substitute.For<IAssetCapitalService>();
            var balances = Substitute.For<IAccountBalanceService>();
            var transactions = Substitute.For<IItemTransactionService>();
            transactions.ListByItem(Arg.Any<ItemTransactionRequest>()).Returns(f => new List<ItemTransactionByItemDto>());
            var builder = new DashboardModelBuilder(balances, transactions, assets);
            var model = builder.CreateModel("");
            Assert.AreEqual(model.CapitalRatio, null);
        }
    }
}
