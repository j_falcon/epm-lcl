namespace Dbsoft.Epm.Test
{
    using Autofac;

    public class TestDatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var config = new EpmConfigBuilder()
                    .WithSetting("Data:ActiveInstance", "Local-CI")
                    .WithConnectionString("EPMContext.Local-CI",
                        "Data Source=.;Initial Catalog=EPM-CI;Integrated Security=True;MultipleActiveResultSets=True")
                    .Build();
                return config;
            });
        }
    }
}