﻿namespace DBSoft.EPMWeb.Support.Filters
{
	using System.Net;
	using System.Web.Mvc;

	public class ErrorHandlerAttribute : HandleErrorAttribute
	{
		public override void OnException(ExceptionContext filterContext)
		{
			if ( !filterContext.ExceptionHandled && filterContext.HttpContext.Request.IsAjaxRequest() && filterContext.Exception != null)
			{
				filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				filterContext.Result = new JsonResult
				{
					JsonRequestBehavior = JsonRequestBehavior.AllowGet,
					Data = new
					{
						filterContext.Exception.Message
					}
				};
				filterContext.ExceptionHandled = true;
				filterContext.HttpContext.Response.Clear();
				filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;      
			}
			else
			{
				base.OnException(filterContext);
			}
		}
	}
}