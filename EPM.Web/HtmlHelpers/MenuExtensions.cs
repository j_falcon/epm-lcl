﻿namespace DBSoft.EPMWeb.HtmlHelpers
{
	using System.Collections.Generic;
	using System.Diagnostics.Contracts;
	using System.Web.Mvc;
	using System.Web.Mvc.Html;

	public static class MenuExtensions
	{
		public static MvcHtmlString BuildMainMenu(this HtmlHelper htmlHelper)
		{
			Contract.Requires(htmlHelper != null);
			var production = new List<string>
			{
				htmlHelper.CreateMenuItem("Refresh API", "RefreshAPI", "Production"),
				htmlHelper.CreateMenuItem("Material", "ProductionMaterial", "Production"),
				htmlHelper.CreateMenuItem("Build Items", "ItemBuild", "Production"),
				htmlHelper.CreateMenuItem("Inbound Contracts", "InboundContract", "Production"),
				htmlHelper.CreateMenuItem("Outbound Contracts", "OutboundContract", "Production"),
				htmlHelper.CreateMenuItem("Post Sell Orders", "MarketRestock", "Production"),
				htmlHelper.CreateMenuItem("Update Sell Orders", "MarketReprice", "Production"),
				htmlHelper.CreateMenuItem("Post Buy Orders", "PurchaseRelist", "Production"),
				htmlHelper.CreateMenuItem("Update Buy Orders", "PurchaseReprice", "Production"),
				htmlHelper.CreateMenuItem("Item Maintenance", "ItemMaintenance", "Production")
			};
			var maintenance = new List<string>
			{
				htmlHelper.CreateMenuItem("Configuration", "Configuration", "Maintenance"),
				htmlHelper.CreateMenuItem("Accounts", "Accounts", "Maintenance")
			};
			var reports = new List<string>
			{
				htmlHelper.CreateMenuItem("Item Sales", "ItemSales", "Reporting"),
				htmlHelper.CreateMenuItem("Daily Sales", "DailySales", "Reporting"),
				htmlHelper.CreateMenuItem("Item Trends", "ItemCostTrend", "Reporting"),
				htmlHelper.CreateMenuItem("Material Trends", "MaterialCostTrend", "Reporting")
			};

			if ( htmlHelper.ViewContext.HttpContext.User.Identity.IsAuthenticated )
			{
				var main = new List<string>
				{
					htmlHelper.CreateMenuItem("Home", "Index", "Home"),
					htmlHelper.CreateMenuItem("Production", "Index", "Production", production),
					htmlHelper.CreateMenuItem("Reporting", "Index", "Reporting", reports),
					htmlHelper.CreateMenuItem("Maintenance", "Index", "Maintenance", maintenance),
					htmlHelper.CreateMenuItem("Logout", "Logout", "Account")
				};
				var menu = CreateMenu(main);
				return new MvcHtmlString(menu);
			}
			else
			{
				var main = new List<string>
				{
					htmlHelper.CreateMenuItem("Login", "Login", "Account"),
					htmlHelper.CreateMenuItem("Register", "Register", "Account")
				};
				var menu = CreateMenu(main);
				return new MvcHtmlString(menu);
			}

		}

		public static string CreateMenu(IEnumerable<string> items)
		{
			var ul = new TagBuilder("ul") {InnerHtml = string.Concat(items)};
			return ul.ToString();
		}

		private static string CreateMenuItem(this HtmlHelper htmlHelper, string text, string action, string controller, 
			IEnumerable<string> subMenu = null)
		{
			var li = new TagBuilder("li");

			if (subMenu == null)
			{
				li.InnerHtml = htmlHelper.ActionLink(text, action, controller).ToHtmlString();
			}
			else
			{
				var routeData = htmlHelper.ViewContext.RouteData;
				var currentAction = routeData.GetRequiredString("action");
				var currentController = routeData.GetRequiredString("controller");
				li.InnerHtml = htmlHelper.ActionLink(text, currentAction, currentController).ToHtmlString();
				li.InnerHtml += CreateMenu(subMenu);
			}
			return li.ToString();
		}
	}
}