﻿namespace DBSoft.EPMWeb
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;
            bundles.Add(new ScriptBundle("~/bundles/js")
                .IncludeDirectory("~/lib", "*.js", true)
                .Include("~/Scripts/jquery.signalR-2.1.2.js")
                .Include("~/Scripts/libs/underscore.js")
                .Include("~/Scripts/typeahead.js")
                );
            bundles.Add(new ScriptBundle("~/bundles/app")
                .Include("~/app/app.js")
                .IncludeDirectory("~/app/Directives", "*.js")
                );


            bundles.Add(new StyleBundle("~/bundles/css")
                .IncludeDirectory("~/lib", "*.css", true)
                .Include("~/Content/site.css")
                );

            BundleTable.EnableOptimizations = false;
        }
    }
}

