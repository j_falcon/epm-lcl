namespace DBSoft.EPMWeb.Models.Reporting
{
    public class ItemSaleTotal
    {
        public decimal GrossAmount { get; set; }
        public decimal? GrossProfit { get; set; }
        public decimal? GPPct
        {
            get
            {
                if ( GrossAmount == 0 )
                {
                    return 0M;
                }
                return GrossProfit * 100 / GrossAmount;
            }
        }
    }
}