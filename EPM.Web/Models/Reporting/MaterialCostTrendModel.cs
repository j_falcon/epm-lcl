﻿namespace DBSoft.EPMWeb.Models.Reporting
{
	using EPM.DAL.DTOs;
    using EPM.UI;
    using System.Collections.Generic;
	using System.Linq;
	using EPM.DAL.Services;
	
	public class MaterialCostTrendModel
	{
		public MaterialCostTrendModel(IMaterialVarianceService service, string token)
		{
			Detail = service.ListVariances(token).OrderByDescending(f => f.Variance);

			TableDef = new TableDefinition();
			TableDef.Columns.Add(new DataColumnDefinition<MaterialCostVarianceDTO>(f => f.MaterialName, caption: "Material"));
			TableDef.Columns.Add(new DataColumnDefinition<MaterialCostVarianceDTO>(f => f.Variance, caption: "Variance"));
		}
		public string Caption
		{
			get
			{
				return "Item Cost Trends";
			}
		}
		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(TableDef, Detail);
			}
		}
		public readonly IEnumerable<MaterialCostVarianceDTO> Detail;
		public TableDefinition TableDef { get; private set; }
	}
}