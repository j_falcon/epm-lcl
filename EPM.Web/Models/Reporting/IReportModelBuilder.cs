namespace DBSoft.EPMWeb.Models.Reporting
{
    using System;

    public interface IReportModelBuilder
    {
        MarketResearchModel CreateMarketResearchModel(string token);
        TransactionAuditModel CreateTransactionAuditModel(string token);
        SubscriberSalesModel CreateSubscriberSalesModel(string token, DateTime? fromDate, DateTime? toDate);
    }
}