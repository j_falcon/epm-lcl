namespace DBSoft.EPMWeb.Models.Reporting
{
    using System.Collections.Generic;

    public class MarketResearchModel
    {
        public MarketResearchModel()
        {
            Detail = new List<MarketResearchDetailModel>();
        }
        public List<MarketResearchDetailModel> Detail { get; set; }
        public string ReasonDisabled { get; set; }

        public static string HelpUrl { get { return "https://dbsoft.atlassian.net/wiki/display/EPM/Market+Research+Report"; } }
    }
}