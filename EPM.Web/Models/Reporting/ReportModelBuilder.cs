namespace DBSoft.EPMWeb.Models.Reporting
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using EPM.DAL;
    using EPM.DAL.CodeFirst.Models;
    using EPM.DAL.Services.Market;
    using EPM.DAL.Services.Transactions;

    public class ReportModelBuilder : IReportModelBuilder
    {
        private readonly IMarketResearchService _service;
        private readonly ITransactionService _transactions;
        private readonly IItemTransactionService _sales;

        public ReportModelBuilder(IMarketResearchService service, ITransactionService transactions, 
            IItemTransactionService sales)
        {
            _service = service;
            _transactions = transactions;
            _sales = sales;
            Mapper.CreateMap<MarketResearchDTO, MarketResearchDetailModel>()
                .ForMember(m => m.Margin, opt => opt.ResolveUsing(r => r.Price - r.Cost))
                .ForMember(m => m.Markup, opt => opt.ResolveUsing(r => r.Cost == 0 ? 0 : (r.Price - r.Cost) / r.Cost * 100))
                ;
            Mapper.CreateMap<ItemTransactionByUserDTO, TransactionAuditDetailModel>();
        }

        public MarketResearchModel CreateMarketResearchModel(string token)
        {
            var mostRecent = _transactions.GetLastTrackedTransaction(token);
            var dateTime = DateTime.UtcNow.AddDays(-7);
            if ( mostRecent == null || mostRecent.DateTime < dateTime )
            {
                return new MarketResearchModel { ReasonDisabled = "The Market Research Report is only available for subscribers with recent history." };
            }
            return new MarketResearchModel
            {
                Detail = Task.Run(() => _service.List(token))
                    .Result
                    .Select(Mapper.Map<MarketResearchDetailModel>)
                    .OrderByDescending(o => o.ProfitFactor).ToList()
            };
        }

        public TransactionAuditModel CreateTransactionAuditModel(string token)
        {
            return new TransactionAuditModel
            {
                Detail = _sales.ListByUser(token)
                    .Select(Mapper.Map<TransactionAuditDetailModel>)
            };
        }

        public SubscriberSalesModel CreateSubscriberSalesModel(string token, DateTime? fromDate, DateTime? toDate)
        {
            var f = fromDate ?? DateTime.UtcNow.StartOfTheDay().AddMonths(-1);
            var t = toDate ?? DateTime.UtcNow.EndOfTheDay().AddDays(-1);
            return new SubscriberSalesModel
            {
                FromDate = f,
                ToDate = t,
                Detail = _sales.ListBySubscriber(new SubscriberTransactionRequest
                {
                    Token = token,
                    DateRange = new DateRange(f, t),
                    TransactionType = TransactionType.Sell
                })
            };
        }
    }
}