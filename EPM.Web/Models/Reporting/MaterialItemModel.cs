﻿namespace DBSoft.EPMWeb.Models.Reporting
{
    using System;
    using System.ComponentModel;

	public class MaterialItemModel
	{
		public MaterialItemModel()
		{
			ReturnUrl = "Index";
		}
		public int ItemID { get; set; }
		[DisplayName("Item")]
		public string ItemName { get; set; }
		[DisplayName("Bounce Factor")]
		public decimal? BounceFactor { get; set; }
		public string ReturnUrl { get; set; }
	}
}