﻿namespace DBSoft.EPMWeb.Models.Reporting
{
    using EPM.DAL.DTOs;
    using EPM.DAL.Services.ItemCosts;
    using EPM.UI;
    using Support.HtmlHelpers;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;

	public class ItemCostTrendModel
	{
		public ItemCostTrendModel(IItemCostService itemCostService, string token)
		{
			_detail = itemCostService.ListVariances(token).OrderByDescending(f => f.Variance);

			_tableDef.Columns.Add(new LinkColumnDefinition<ItemCostVarianceDTO>(f => f.ItemName, CreateLink));
			_tableDef.Columns.Add(new DataColumnDefinition<ItemCostVarianceDTO>(f => f.Variance));
		}
		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, _detail);
			}
		}

        public static string HelpUrl { get { return "https://dbsoft.atlassian.net/wiki/display/EPM/Item+Cost+Trends"; } }

	    private static LinkDefinition CreateLink(object rec)
		{
			var dsd = rec as ItemCostVarianceDTO;
			Debug.Assert(dsd != null, "dsd != null");
			var linkDef = HtmlUtilities.CreateLinkDefinition<ItemCostVarianceDTO>(
				rec, 
				() => dsd.ItemName,
				"ItemCostTrendDetail",
				"Reporting",
				new { dsd.ItemName });
			return linkDef;
		}

		private readonly IEnumerable<ItemCostVarianceDTO> _detail;
		private readonly TableDefinition _tableDef = new TableDefinition();
	}
}