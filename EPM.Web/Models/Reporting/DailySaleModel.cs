﻿using DBSoft.EPM.UI;

namespace DBSoft.EPMWeb.Models.Reporting
{
	using AutoMapper;
	using EPM.DAL;
	using EPM.DAL.CodeFirst.Models;
	using EPM.DAL.DTOs;
	using EPM.DAL.Requests;
	using EPM.DAL.Services.Transactions;
	using EPMWeb;
	using Support.HtmlHelpers;
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using System.Web.Routing;

	public class DailySaleModel
	{
		public DailySaleModel(IItemTransactionService service, string token)
		{
			Mapper.CreateMap<ItemTransactionByDateDTO, DailySaleItemModel>();

			Detail = Mapper.Map<IEnumerable<ItemTransactionByDateDTO>, IEnumerable<DailySaleItemModel>>(service
				.ListByDate(new ItemTransactionRequest 
				{ 
					Token = token, 
					DateRange = new DateRange(),
					TransactionType = TransactionType.Sell
				})
				.OrderByDescending(o => o.DateTime));
		}
		public IEnumerable<DailySaleItemModel> Detail { get; private set; }
// ReSharper disable UnusedMember.Global
		public DateTime FromDate
		{
			get
			{
				return Detail.Min(f => f.DateTime);
			}
		}
		public DateTime ToDate
		{
			get
			{
				return Detail.Max(f => f.DateTime);
			}
		}
		public DailySaleTotal Total
		{
			get
			{
				return new DailySaleTotal
				{
					Sales = Detail.Sum(f => f.GrossAmount),
					Profit = Detail.Sum(f => f.GPAmt)
				};
			}
		}
		public string TableHtml
		{
			get
			{
				if ( _tableDef == null )
				{
					_tableDef = new TableDefinition();
					_tableDef.Columns.Add(new LinkColumnDefinition<DailySaleItemModel>(f => f.DateTime,
						ItemSalesLink));
					_tableDef.Columns.Add(new DataColumnDefinition<DailySaleItemModel>(f => f.GrossAmount, null, new FooterDefinition<DailySaleTotal>(f => f.Sales)));
					_tableDef.Columns.Add(new DataColumnDefinition<DailySaleItemModel>(f => f.GPAmt, null, new FooterDefinition<DailySaleTotal>(f => f.Profit)));
					_tableDef.Columns.Add(new DataColumnDefinition<DailySaleItemModel>(f => f.GPPct, null, new FooterDefinition<DailySaleTotal>(f => f.GPPct)));
				}
				return TableHtmlGenerator.Generate(_tableDef, Detail, Total);
			}
		}

		private static LinkDefinition ItemSalesLink(object rec)
		{
			var dsd = rec as DailySaleItemModel;
			Debug.Assert(dsd != null, "dsd != null");
			var linkDef = HtmlUtilities.CreateLinkDefinition<DailySaleItemModel>(rec, "DateTime", "ItemSalesRange", "Reporting",
				new RouteValueDictionary(
				new
				{
					FromDate = dsd.DateTime,
					ToDate = dsd.DateTime
				}));
			return linkDef;
		}
		private TableDefinition _tableDef;
	}
	public class DailySaleTotal
	{
		public decimal Sales { get; set; }
		public decimal? Profit { get; set; }
		public decimal? GPPct
		{
			get
			{
				return Sales == 0 ? null : Profit * 100 / Sales;
			}
		}
	}
}