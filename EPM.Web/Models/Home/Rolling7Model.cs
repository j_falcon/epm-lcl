﻿namespace DBSoft.EPMWeb.Models.Home
{
	using System;
	using EPM.DAL;
	using EPM.DAL.CodeFirst.Models;
	using EPM.DAL.DTOs;
	using EPM.DAL.Interfaces;
	using EPM.DAL.Services;
	using EPM.DAL.Services.Transactions;
	using EPM.UI;
    using System.Collections.Generic;
	using System.Linq;


	public class Rolling7Model
	{
		private readonly IAccountBalanceService _accountBalanceService;
		private readonly IItemTransactionService _itemTransactionService;
		private readonly IAssetCapitalService _assetCapitalService;
	    private readonly string _token;

		public Rolling7Model(IAccountBalanceService accountBalanceService, IItemTransactionService itemTransactionService, 
			IAssetCapitalService assetCapitalService, IEpmConfig epmConfig, string token)
		{
		    _accountBalanceService = accountBalanceService;
			_itemTransactionService = itemTransactionService;
			_assetCapitalService = assetCapitalService;
		    _token = token;

			var tableDef = new TableDefinition(displayHeader: false)
			{
				Columns = new List<IColumnDefinition>
				{
					new DataColumnDefinition<Rolling7Detail>(f => f.Title),
					new DataColumnDefinition<Rolling7Detail>(f => f.Amount)
				}
			};
			TableModel = new TableModel
			{
				Detail = Detail,
				TableDef = tableDef
			};
            Version = epmConfig.GetBuildVersion();
            ReleaseNotesLink = string.Format("https://dbsoft.atlassian.net/issues/?jql=project%20%3D%20EVEEPM%20AND%20fixVersion%20%3D%20{0}%20ORDER%20BY%20updated%20DESC%2C%20priority%20DESC%2C%20created%20ASC", Version);
		}

        public string Version { get; private set; }
        public TableModel TableModel { get; private set; }
        public string ReleaseNotesLink { get; private set; }

		private IEnumerable<Rolling7Detail> Detail
		{
			get
			{
				var result = new Rolling7Result
				{
				};
				return new List<Rolling7Detail>
			{
				new Rolling7Detail { Title = "Profit/7", Amount = result.Profit},
				new Rolling7Detail { Title = "Sales/7", Amount = result.Sales},
				new Rolling7Detail { Title = "GP%/7", Amount = result.GPPct},
				new Rolling7Detail { Title = "Profit/30", Amount = result.Profit30},
				new Rolling7Detail { Title = "Sales/30", Amount = result.Sales30},
				new Rolling7Detail { Title = "GP%/30", Amount = result.GPPct30},
				new Rolling7Detail { Title = "Cash", Amount = result.WalletBalance},
				new Rolling7Detail { Title = "Assets", Amount = result.Capital},
				new Rolling7Detail { Title = "Capital/Sales/30 Ratio", Amount = result.CapitalRatio}
			};
			}
		}

		private decimal GetCapital()
		{
		    var items = _assetCapitalService.ListItemCapital(_token).Sum(f => f.TotalValue);
		    var materials = _assetCapitalService.ListMaterialCapital(_token).Sum(f => f.FactoryValue + f.MarketValue + f.RemoteValue);
		    return items + materials;
		}

	    private IEnumerable<ItemTransactionByItemDTO> GetTransactions(int numDays = 7)
		{
			return _itemTransactionService.ListByItem(new ItemTransactionRequest
			{
				Token = _token,
				TransactionType = TransactionType.Sell,
				DateRange = new DateRange { StartDate = DateTime.UtcNow.AddDays(-numDays), EndDate = DateTime.UtcNow.AddDays(-1) }
			});
		}
		
		private decimal GetSales30()
		{
			return GetTransactions(30).Sum(f => f.GrossAmount);
		}

		private decimal? GetProfit30()
		{
			return GetTransactions(30).Sum(f => f.GPAmt);
		}

		private decimal? GetCapitalRatio()
		{
			var sales = GetSales30();
			return sales == 0 ? (decimal?)null : (GetCapital() + GetWalletBalance()) / sales;
		}

		private decimal GetSales()
		{
			return GetTransactions().Sum(f => f.GrossAmount);
		}

		private decimal? GetProfit()
		{
			return GetTransactions().Sum(f => f.GPAmt);
		}

		private decimal GetWalletBalance()
		{
			return _accountBalanceService
				.List(_token)
				.Where(f => f.AccountKey == AccountBalanceService.MasterWallet)
				.Sum(f => f.Balance);
		}

		private class Rolling7Result
		{
			public decimal Sales { get; set; }
			public decimal? Profit { get; set; }
			public decimal WalletBalance { get; set; }
			public decimal? CapitalRatio { get; set; }
			public decimal? GPPct
			{
				get
				{
					if (Sales == 0)
					{
						return 0;
					}
					return Profit * 100/ Sales;
				}
			}
			public decimal Capital { get; set; }
			public decimal? Profit30 { get; set; }
			public decimal? Sales30 { get; set; }
			public decimal? GPPct30
			{
				get
				{
					if (Sales30 == 0)
					{
						return 0;
					}
					return Profit30 * 100 / Sales30;
				}
			}
		}
	}
	public class Rolling7Detail
	{
		public string Title { get; set; }
		public decimal? Amount { get; set; }
	}
}