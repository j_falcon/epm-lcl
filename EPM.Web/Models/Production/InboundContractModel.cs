﻿namespace DBSoft.EPMWeb.Models.Production
{
    using AutoMapper;
    using EPM.DAL.Services.Contracts;
    using EPM.UI;
    using System.Collections.Generic;

    public class InboundContractModel
	{
		private readonly TableDefinition _tableDef;

		public InboundContractModel(IContractService service, string token)
		{
			Mapper.CreateMap<InboundContractDTO, InboundContractItemModel>();

			_detail = Mapper.Map<IEnumerable<InboundContractDTO>, IEnumerable<InboundContractItemModel>>(service.ListInboundContracts(token));
			_tableDef = new TableDefinition();
			_tableDef.Columns.Add(new DataColumnDefinition<InboundContractItemModel>(f => f.StationName));
			_tableDef.Columns.Add(new DataColumnDefinition<InboundContractItemModel>(f => f.Value));
		}

		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, _detail);
			}
		}

		private readonly IEnumerable<InboundContractItemModel> _detail;
	}
}