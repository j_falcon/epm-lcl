﻿namespace DBSoft.EPMWeb.Models.Production
{
    using System.Collections.Generic;

    public class RefreshApiModel
	{
		public string HelpUrl { get { return "https://dbsoft.atlassian.net/wiki/display/EPM/Refresh+API"; } }
	}
}