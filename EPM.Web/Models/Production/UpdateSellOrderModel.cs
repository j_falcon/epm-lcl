﻿namespace DBSoft.EPMWeb.Models.Production
{
    using System;
    using EPM.DAL.Interfaces;
    using EPM.UI;
    using AutoMapper;
	using EPM.DAL.CodeFirst.Models;
	using EPM.DAL.DTOs;
	using EPM.DAL.Requests;
	using System.Collections.Generic;
	using System.Linq;

    public class UpdateSellOrderModel
	{
		public UpdateSellOrderModel(IMarketRepriceService service, string token)
		{
			Mapper.CreateMap<MarketRepriceDTO, UpdateSellOrderItemModel>();

			Detail = Mapper.Map<IEnumerable<MarketRepriceDTO>, IEnumerable<UpdateSellOrderItemModel>>(service.List(new MarketRepriceRequest 
			{ 
				Token = token,
				OrderType = OrderType.Sell 
			})
				.Where(f => f.ListedPrice != f.NewPrice)
				.OrderBy(f => f.ItemName));

			_tableDef.Columns.Add(new DataColumnDefinition<UpdateSellOrderItemModel>(f => f.ItemName));
			_tableDef.Columns.Add(new DataColumnDefinition<UpdateSellOrderItemModel>(f => f.NewPrice));
			_tableDef.Columns.Add(new DataColumnDefinition<UpdateSellOrderItemModel>(f => f.MarketPrice));
			_tableDef.Columns.Add(new DataColumnDefinition<UpdateSellOrderItemModel>(f => f.ListedPrice));
			_tableDef.Columns.Add(new DataColumnDefinition<UpdateSellOrderItemModel>(f => f.Markup));
		}

	    private static string GetClass(object rec)
	    {
	        var model = rec as UpdateSellOrderItemModel;
            if ( model != null && model.Timestamp < DateTime.UtcNow.AddHours(-4))
            {
                return "class='error'";
            }
            return "";
	    }

	    public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, Detail);
			}
		}
        private readonly TableDefinition _tableDef = new TableDefinition
        {
            GetClass = GetClass
        };
		private IEnumerable<UpdateSellOrderItemModel> Detail {get;set;}

		public static string HelpUrl
		{
			get { return "https://dbsoft.atlassian.net/wiki/display/EPM/Update+Sell+Orders"; }
		}
	}
}