﻿namespace DBSoft.EPMWeb.Models.Production
{
    using System.ComponentModel.DataAnnotations;

    public class InboundContractItemModel
    {
        public string StationName { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###}")]
        public decimal Value { get; set; }
    }
}