﻿namespace DBSoft.EPMWeb.Models
{
	public class RefreshMarketDetails : IRefreshDetails
	{
		public string Div { get { return "marketstatus"; } }
		public string Action { get { return "MarketStatus"; } }
		public string Title { get { return "Refresh Market"; } }
		public string TitleDiv { get { return "market-box"; } }
	}
}