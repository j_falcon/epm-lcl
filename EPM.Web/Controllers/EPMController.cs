﻿namespace DBSoft.EPMWeb.Controllers
{
    using System.Web.Mvc;
	using Newtonsoft.Json;
	using NLog;
	using LogManager = NLog.LogManager;

    public class EpmController : Controller
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		protected override void OnException(ExceptionContext context)
		{
		    if (context.ExceptionHandled) return;
		    Logger.Error(context.Exception, "Exception caught in base controller");
            var result = RedirectToAction("Login", "Account", new { ErrorMessage = context.Exception.Message });
		    context.Result = result;
		    context.ExceptionHandled = true;
		}

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			Token = AuthenticationHelper.GetAuthenticationTicket(HttpContext);
            IsEveClient = HttpContext.Request.ServerVariables["HEADER_EVE_TRUSTED"] == "Yes";
            ViewBag.Token = Token;
            ViewBag.HideMenu = false;
			base.OnActionExecuting(filterContext);
		}

	    protected bool IsEveClient { get; set; }

	    protected string Token { get; private set; }
	}

    public class JsonNetResult : JsonResult 
    {
        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = "application/json";
            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;
            if (Data == null) return;
            var writer = new JsonTextWriter(response.Output) { Formatting = Formatting.Indented };
            var serializer = JsonSerializer.Create(new JsonSerializerSettings());
            serializer.Serialize(writer, Data);
            writer.Flush();
        }

    }
}