﻿
namespace DBSoft.EPMWeb.Infrastructure
{
    using System.Collections.Generic;
    using EPM.DAL.Services;
    using EPM.Logic;
    using Microsoft.AspNet.SignalR;

    public class ImportNotifier : IImportNotifier
    {
        private readonly IUserService _users;
        private readonly List<string> _running;

        public ImportNotifier(IUserService users)
        {
            _users = users;
            _running = new List<string>();
        }

        public void Start(string token, string name)
        {
            _running.Add(name);
            Notify(token);
        }

        public void Stop(string token, string name)
        {
            _running.RemoveAll(f => f == name);
            Notify(token);
        }

        private void Notify(string token)
        {
            var user = _users.GetAuthenticatedUser(token);

            GlobalHost
               .ConnectionManager
               .GetHubContext<RefreshHub>()
               .Clients.User(user.UserName).RefreshUpdated(_running);
        }
    }
}
