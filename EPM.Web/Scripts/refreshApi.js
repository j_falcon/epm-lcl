﻿var refreshApi = {
	lastLoad: 0,

	isRunning: false,
	
	doLoad: function (ctx, action) {
		var now = (new Date()).getTime();
		if (refreshApi.isRunning || now - refreshApi.lastLoad > 600000) {
			$(ctx).load(action, function () {
				refreshApi.checkSpinner(ctx);
			});
			refreshApi.lastLoad = now;
		}
	},
	checkSpinner: function (ctx) {
		var status = $(ctx).find("div[data-status]");
		if (status.data('status') == 'Running') {
			refreshApi.setSpinner(status);
			refreshApi.isRunning = true;
		} else {
			refreshApi.isRunning = false;
		};
	},
	/* Activate the spinner */
	setSpinner: function (ctx) {
		$(ctx).find("span").addClass('spinning');
	},
	/* If the import isn't running start the spinner, update the button and send the import request */ 
	doRefresh: function (ctx, action, text) {
		if (!$(ctx).find("span.spinning").length) {
			refreshApi.setSpinner(ctx);
			$(ctx).find("span").text(text);
			refreshApi.isRunning = true;
			$.get(action);
		}
	}
};

